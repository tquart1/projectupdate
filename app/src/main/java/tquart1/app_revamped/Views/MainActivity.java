package tquart1.app_revamped.Views;

import android.app.Activity;
import android.net.Uri;

import android.os.Bundle;

import tquart1.app_revamped.Interfaces.IMainPresenter;
import tquart1.app_revamped.Interfaces.IMainView;
import tquart1.app_revamped.R;
import android.content.Intent;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;



import org.w3c.dom.Text;

public class MainActivity extends Activity implements View.OnClickListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    IMainPresenter presenter;
    // widget
    Button backButton;
    Button forwardButton;
    TextView chosenColorText;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        forwardButton = (Button) findViewById(R.id.chosen_button_forward);
        forwardButton.setOnClickListener(MainActivity.this);
        backButton = (Button) findViewById(R.id.chosen_button_back);
        backButton.setOnClickListener(MainActivity.this);

    }

    public Button getForwardButton() {
        return forwardButton;
    }

    public Button getBackButton() {
        return backButton;
    }

    public void setPresenter(IMainPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onClick(View view) {

        switch(view.getId()) {
            case R.id.chosen_button_forward:

                Log.d(TAG, "You moved Forward!");
                break;
            case R.id.chosen_button_back:
                Log.d(TAG, "You moved Backward");
                break;
        }
    }

public boolean checkMethods(String t){

    return false;
}
}
