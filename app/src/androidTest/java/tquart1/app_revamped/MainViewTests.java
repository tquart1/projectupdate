package tquart1.app_revamped;

import android.support.test.filters.SmallTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;


import tquart1.app_revamped.Interfaces.IMainPresenter;
import tquart1.app_revamped.Views.MainActivity;

/**
 * Created by user on 2/18/2017.
 */

public class MainViewTests extends ActivityInstrumentationTestCase {
    @Rule



    IMainPresenter presenter;

    public MainViewTests(String pkg, Class activityClass) {
        super(pkg, activityClass);
    }
   protected void setUp() throws Exception{
       super.setUp();
   }
    @SmallTest
    public void editText(){
        TextView tv = (TextView) getActivity().findViewById(R.id.textView2);
        assertNotNull(tv);
    }
    public void testButtonForward(){
        Button bt = (Button)getActivity().findViewById(R.id.chosen_button_forward);
        assertNotNull(bt);
    }
    public void testButtonBackward(){
        Button bt2 = (Button)getActivity().findViewById(R.id.chosen_button_back);
        assertNotNull(bt2);
    }
    public void setPresenter(){
        presenter = this.presenter;

        }



}
